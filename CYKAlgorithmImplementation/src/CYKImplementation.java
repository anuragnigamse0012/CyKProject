import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class CYKImplementation {
	
	   private HashMap<String, List<String[]>> variables;

	   private HashMap<String, List<String>> terminals;
	      
	   List<String> ruleSymbols = new ArrayList<String>();
	   
	   Map<String,List<String[]>> editVariables = new HashMap<String,List<String[]>>();
	   
	   private static int character = 'A';
	   
	   private static ArrayList<String>[][] table;
	   
	   int i = 0;
	   
	   private static String startVariable;
	   
	   private static String startSymbol;
	   
	   Map<Integer,String> derivations = new HashMap<Integer,String>();
	
	   public CYKImplementation() {
		   	  variables = new HashMap<String, List<String[]>>();
		      terminals = new HashMap<String, List<String>>();
	   }
	
	
	  private static CYKImplementation cykImplement;
	  private static BufferedReader br;
	  
	  public static CYKImplementation getInstance() {
		    cykImplement = new CYKImplementation();
		    return cykImplement;
	  }
	  
	  public void processGrammarFile(String file)
	   {
		  
	      File grammarFile = null;
	      Scanner scanner = null;
	      try
	      {
	         grammarFile = new File(file);
	         scanner = new Scanner(grammarFile);
	         startSymbol =scanner.nextLine().trim();
	         String[] line = scanner.nextLine().split("->");
	         startVariable = line[0].trim();
	         ruleSymbols.add(startVariable);
	         
	         do
	         {
	            String variable = line[0].trim(); 
	            ruleSymbols.add(variable);
	            if(line[1].contains("'"))
	            {
	            	List<String> terminalArr;
	            	String terminal = line[1].replace("'", "");
	            	if(terminals.containsKey(variable)){
	            		terminalArr = terminals.get(variable);
	            		terminalArr.add(terminal.trim());
	            		terminals.put(variable, terminalArr);
	            	}else{
	            		terminalArr = new ArrayList<String>();
	            		terminalArr.add(terminal.trim());
	            		terminals.put(variable, terminalArr);
	            	}
	            	
	            }
	            else
	            {
	               String[] rest = line[1].trim().split(" ");
	               
	               if (rest != null)
	               {
	            	  if(variables.containsKey(variable)){
	            		  List<String[]> lstDerivation = variables.get(variable);
	            		  lstDerivation.add(rest);
	             		  variables.put(variable, lstDerivation);
	            	  }else{
	            		 List<String[]> lstDerivation = new ArrayList<String[]>();  
	            		 lstDerivation.add(rest);
	            		 variables.put(variable, lstDerivation);
	            	  }
	                 
	               }
	            }
	            if (scanner.hasNextLine())
	               line = scanner.nextLine().split("->");
	            else
	               line = null;
	         } while (line != null);
	         scanner.close();
	      }
	      catch (IOException ex)
	      {
	         ex.printStackTrace();
	      }
	     
	}
	  
	public void convertCfgtoCnf() {
		  initialStateonRightHandSide();
		  removeNullProductions();
		  removeUnitProductions();
		  removeMoreThanTwoVariblesInRHS();
			  
	}
	
	public void removeUnitProductions() {
		Map<String,List<String[]>> map = new HashMap<String,List<String[]>>();;
		for(Iterator<Map.Entry<String, List<String[]>>> nonTerminal = variables.entrySet().iterator();nonTerminal.hasNext();) {
			Map.Entry<String, List<String[]>> entry = nonTerminal.next();
			String key = null;
			int i = 0;
			boolean flag = false;
			List<String[]> list = new ArrayList<String[]>();
			for(String[] arr : entry.getValue()) {
				key = entry.getKey();
				if(arr.length == 1) {
					flag = true;
				}
				else {
					list.add(variables.get(entry.getKey()).get(i));
				}
				i++;
			}
			
			if(!list.isEmpty() && flag) {
				map.put(key, list);
				nonTerminal.remove();
			}
			
		}
		if(!map.isEmpty()) {
			for(Map.Entry<String, List<String[]>> entry : map.entrySet()) {
				variables.put(entry.getKey(), entry.getValue());
			}
		}
		
	}
	
	@SuppressWarnings("unlikely-arg-type")
	public void removeMoreThanTwoVariblesInRHS() {
		String key = null;
		Map<String,List<String[]>> newMap = new HashMap<String,List<String[]>>();
		List<String[]> list = null;
		
		String newCharacter = Character.toString((char)character);
		Map<String,String[]> addMap = new HashMap<String,String[]>();
		for(Iterator<Map.Entry<String, List<String[]>>> nonTerminal = variables.entrySet().iterator();nonTerminal.hasNext();) {
			Map.Entry<String, List<String[]>> entry = nonTerminal.next();
			boolean flag = false;
			int i = 0;
			 
			 list = new ArrayList<String[]>();
			for(String[] arr : entry.getValue()) {
				List<String> item = new ArrayList<String>(Arrays.asList(arr));
				key = entry.getKey();
				if(item.size() <= 2) {
					list.add(variables.get(entry.getKey()).get(i));
					
				}
				else {
					flag = true;
					newMap.put(key, list);
					
					while(item.size() > 2) {
						int size = item.size();
					  String[] value = {item.get(size-2),item.get(size-1)};
						while(ruleSymbols.contains(newCharacter)) {
						  character++;
						  newCharacter = Character.toString((char)character);
					}
						ruleSymbols.add(newCharacter);
					
						
						item.removeAll(item.subList(size-2, size));
						item.add(newCharacter);
						addMap.put(newCharacter,value);
					}
					String[] stringArray = item.toArray(new String[0]);
					List<String[]> l = new ArrayList<String[]>();
					l.add(stringArray);
					if(newMap.containsKey(key)){
		          		  List<String[]> lstDerivation = newMap.get(key);
		          		  lstDerivation.addAll(l);
		           		  newMap.put(key, lstDerivation);
		          	  }else{
		          		 List<String[]> lstDerivation = new ArrayList<String[]>();  
		          		 lstDerivation.addAll(l);
		          		 newMap.put(key, lstDerivation);
		          	  }
					
					
					
					
				}
				i++;
			}
			if(flag) {
				nonTerminal.remove();
			}
			
		}
		
		if(newMap != null) {
		   	for(Map.Entry<String, List<String[]>> m : newMap.entrySet()) {
		   		if(variables.containsKey(m.getKey())){
          		  List<String[]> lstDerivation = variables.get(m.getKey());
          		  lstDerivation.addAll(m.getValue());
           		  variables.put(m.getKey(), lstDerivation);
          	  }else{
          		 List<String[]> lstDerivation = new ArrayList<String[]>();  
          		 lstDerivation.addAll(m.getValue());
          		 variables.put(m.getKey(), lstDerivation);
          	  }
		   	
		   	}
		   	
		   	for(Map.Entry<String, String[]> m : addMap.entrySet()) { 
		   	   List<String[]> li = new ArrayList<String[]>();
		   	   li.add(m.getValue());
		   	   variables.put(m.getKey(),li);
		   	}
		}
		for(Map.Entry<String, List<String>> term : terminals.entrySet()) {
	    	  System.out.println(term.getKey()+"\t"+term.getValue());
	      }
	      
	      for(Map.Entry<String, List<String[]>> term1 : variables.entrySet()) {
	    	  
	    	  for(String[] arr : term1.getValue()) {
	    		 System.out.println(term1.getKey()+"\t"+Arrays.toString(arr));
	    		 }
	      }
		
	}
	  
	public void initialStateonRightHandSide() {
		List<String[]> array = new ArrayList<String[]>();
		for(Map.Entry<String, List<String[]>> nonTerminal : variables.entrySet()) {
		if(nonTerminal.getKey().equals(startSymbol)) {
			  array.addAll(nonTerminal.getValue());
		   }
		}
		String newStart = Character.toString((char)character);
		for(Map.Entry<String, List<String[]>> nonTerminal : variables.entrySet()) {
			   
			  for(String[] arr : nonTerminal.getValue()) {
				  
	    		   if((Arrays.toString(arr)).contains(startSymbol)) {
	    			   while(ruleSymbols.contains(newStart)) {
	    				   character++;
	    				   newStart = Character.toString((char)character);
	    			   }
	    			   
	    			   String[] v = {startSymbol};
	    			   List<String[]> value = new ArrayList<String[]>();
	    			   value.add(v);
	    			   variables.put(newStart, array);
	    			   break;
	    		   }
	    		  
	    	   }
	    	 break; 
	      }
	}
	
	public void removeNullProductions() {
		
		for(Iterator<Map.Entry<String, List<String[]>>> nonTerminal = variables.entrySet().iterator();nonTerminal.hasNext();) {
			Map.Entry<String, List<String[]>> entry = nonTerminal.next();
			
			for(String[] arr : entry.getValue()) {
				String key = entry.getKey();
				if(arr.length == 1 &&  arr[0].equals("")) {
					System.out.println("Print key:"+key);
					  
					  replaceOtherTerminals(key);
					  IfnotInOtherTerminals(key);
			}
		}
			
}
		for(Iterator<Map.Entry<String, List<String[]>>> nonTerminal = variables.entrySet().iterator();nonTerminal.hasNext();) {
			Map.Entry<String, List<String[]>> entry = nonTerminal.next();
			
			for(String[] arr : entry.getValue()) {
				String key = entry.getKey();
				if(arr.length == 1 &&  arr[0].equals("")) {
					nonTerminal.remove();
			}
		}
			
}
		
		
		for(Map.Entry<String, List<String[]>> m : editVariables.entrySet()) {
			
			if(variables.containsKey(m.getKey())){
        		  List<String[]> lstDerivation = variables.get(m.getKey());
        		  lstDerivation.addAll(m.getValue());
         		  variables.put(m.getKey(), lstDerivation);
        	  }else{
        		 List<String[]> lstDerivation = new ArrayList<String[]>();  
        		 lstDerivation.addAll(m.getValue());
        		 variables.put(m.getKey(), lstDerivation);
        	  }
		}
		
		
}
	
	public void IfnotInOtherTerminals(String key) {
		boolean flag = false;
		for(Iterator<Map.Entry<String, List<String[]>>> nonTerminal = variables.entrySet().iterator();nonTerminal.hasNext();) {
			Map.Entry<String, List<String[]>> entry = nonTerminal.next();
			  if(!entry.getValue().contains(key)) {
				  flag = true;
				  
			  }
			  else {
				  flag = false;
			  }
			 
		}
		if(flag) {
			List<String[]> arr = new ArrayList<String[]>(); 
			
			arr = variables.get(key);
			for(String[] array : arr) {
				if(array[0].equals("")) {
					
				}
				else {
					List<String[]> arrTerm = new ArrayList<String[]>();
					arrTerm.add(array);
					if(editVariables.containsKey(key)){
		        		  List<String[]> lstDerivation = editVariables.get(key);
		        		  lstDerivation.addAll(arrTerm);
		         		  editVariables.put(key, lstDerivation);
		        	  }else{
		        		 List<String[]> lstDerivation = new ArrayList<String[]>();  
		        		 lstDerivation.addAll(arrTerm);
		        		 editVariables.put(key, lstDerivation);
		        	  }
				}
			}
		}
		
	}
	
	@SuppressWarnings({ "null", "unlikely-arg-type" })
	public void replaceOtherTerminals(String key) {
		 
		 List<String[]> newValue = new ArrayList<String[]>();
		 /*editedVariables = new HashMap<String, List<String[]>>();
		 editedTerminals = new HashMap<String, List<String>>();*/
		 for(Iterator<Map.Entry<String, List<String[]>>> nonTerminal = variables.entrySet().iterator();nonTerminal.hasNext();) {
				Map.Entry<String, List<String[]>> entry = nonTerminal.next();
		
			int count = 0 ;
		      int i =0;
			   for(String[] arr : entry.getValue()) {
				 if(!arr[0].equals("")) {
				  char[] ch = arr[i].toCharArray();
				    for(int j=0;j<ch.length;j++) {
				    	String cha = Character.toString(ch[j]).trim();
				    	if(ch.length == 1 && cha.equals(key)) {
				    		directVariables(entry.getKey());
				    	}
				    	if(cha.equals(key) && ch.length > 1) {
				    		count++;
				    	}
				    }
				    
				 }
				 if(count > 0) {   
					  convertVariables(key,count,entry.getValue().get(i),entry.getKey());
					   
				   	}
			  
			  
			}	 
			   
		}
}
	
	public void directVariables(String key) {
		
		for(Map.Entry<String, List<String[]>> m : variables.entrySet()) {
			String keyValue = m.getKey();
			List<String[]> listD = new ArrayList<String[]>();
		     for(String[] list : m.getValue()) {
		    	 boolean flag = false;
		    	   char[] ch = list[i].toCharArray();
		    	   String s = "";
		    	   
		    	    for(int i = 0; i<ch.length; i++) {
		    	    	 String cha = Character.toString(ch[i]);
		    	    	   if(!cha.equals(key)) {
		    	    		   //do nothing
		    	    		   s = s.trim()+cha;
		    	    	   }
		    	    	   else {
		    	    		    if(i == (ch.length)-1) {
		    	    		    	listD.add(list);
		    	    		    }
		    	    		   flag = true;
		    	    	   }
		    	    
		    	    }
		    	    if(flag) {
		    	    String[] str = {s};
		    	    listD.add(str);
		    	    }
		     }
		     for(String[] edit: listD) {
					
					if(editVariables.containsKey(keyValue)){
		        		  List<String[]> lstDerivation = editVariables.get(keyValue);
		        		  lstDerivation.add(edit);
		         		  editVariables.put(keyValue, lstDerivation);
		        	  }else{
		        		 List<String[]> lstDerivation = new ArrayList<String[]>();  
		        		 lstDerivation.add(edit);
		        		 editVariables.put(keyValue, lstDerivation);
		        	  }
				}
				
		}
		
	}
	
	public void convertVariables(String key, int count, String[] value, String keyValue) {
		
		int l = 0 ;
		
		List<String[]> arr = new ArrayList<String[]>();
		List<Integer> ar = new ArrayList<Integer>();
		
		
		for(String val : value) {
		    if(!(val.length() == 1)) {
			int k = val.indexOf(key);
			ar.add(k);
			while(l < count) {
				String anotherValue="";
				for(int i = 0 ; i< val.length(); i++) {
					if(i != k ) {
						anotherValue = anotherValue.trim() + val.charAt(i);
					}
				}
				k = val.indexOf(key, k+1);
				ar.add(k);
				l++;
				String[] addVal = {anotherValue};
				arr.add(addVal);
			}	
			String anotherString = "";
			
			
			for(int i = 0; i< val.length();i++) {
				if(ar.contains(i)) {
					
				}
				else { 
					anotherString  = anotherString.trim() + val.charAt(i);
				}
			}
			
			
			String[] addVal = {anotherString};
			arr.add(addVal);
		}
		for(String[] edit: arr) {
			
			if(editVariables.containsKey(keyValue)){
        		  List<String[]> lstDerivation = editVariables.get(keyValue);
        		  lstDerivation.add(edit);
         		  editVariables.put(keyValue, lstDerivation);
        	  }else{
        		 List<String[]> lstDerivation = new ArrayList<String[]>();  
        		 lstDerivation.add(edit);
        		 editVariables.put(keyValue, lstDerivation);
        	  }
		}
	}
		
}
	
	@SuppressWarnings("unchecked")
	   public boolean processString(String[] w)
	   {
		  Map<String,List<String>> hmap = new HashMap<String,List<String>>();
		  List<String> list = new ArrayList<String>();
		  StringBuffer outStr = new StringBuffer();
		  int length = w.length;
	      table = new ArrayList[length][];
	      for (int i = 0; i < length; ++i)
	      {
	         table[i] = new ArrayList[length];
	         for (int j = 0; j < length; ++j)
	            table[i][j] = new ArrayList <String>();
	      } 
	      for (int i = 0; i < length; ++i)
	      {
	         Set<String> keys = terminals.keySet();
	         
	         for (String key : keys)
	         {
	            if (terminals.get(key).contains(w[i])) {
	            	table[i][i].add(key.trim());
	            	if(hmap.containsKey(key)) {
	            		List<String> lst  = hmap.get(key);
	            		lst.add(w[i]);
	            		hmap.put(key, lst);
	            	}
	            	else {
	            	List<String> lst = new ArrayList<String>();
	            	lst.add(w[i]);
	            	hmap.put(key,lst);
	            	}
	            	//list.add("("+key+" "+w[i]+")");
	                
	            }
	         }
	         //System.out.println(table[i][i]);
	      }
	      int countDerivative=0;
	      for (int l = 2; l <= length; ++l)
	      {
	    	  
	         for (int i = 0; i <= length - l; ++i)
	         {
	            int j = i + l - 1;
	            for (int k = i; k <= j - 1; ++k)
	            {
	               //System.out.println("i: "+i+" j : "+j+" k : "+k);
	               Set<String> keys = variables.keySet();
	               //System.out.println(table[i][k] + " :::: "+ table[k + 1][j]);
	               for (String key : keys)
	               {
	            	  List<String[]> listDerivatives = variables.get(key);
	                  for(String[] values:listDerivatives){
	                	  //System.out.println("key : "+ key + " value: "+ values[0]  + " "+  values[1] );
	                	  if (table[i][k].contains((values[0]))
	                              && table[k + 1][j].contains(values[1])){
	                		  countDerivative++;
	                		  //System.out.println("Add at  table["+i+"]["+ j+"] : "+key);
	                		  derivations.put(countDerivative, key.trim()+"->"+values[0]+" "+values[1]);
	                		  table[i][j].add(key.trim());
	                		  
	                	  }
	                           
	                  }
	            	  
	               }
	            }
	         }
	      }
	       List<String> listSet = new ArrayList<String>();
	       int size = hmap.size();
	       int inr = 1;
	      for(Map.Entry<Integer, String> map : derivations.entrySet()) {
	                 String add = "";
	           if(inr<size) {
	    	         String[] word = map.getValue().split("->");
	    		     String[] words = word[1].split(" ");
	    		     for(int i =0; i<words.length; i++) {
	    		    	 for(Map.Entry<String, List<String>>m : hmap.entrySet()) {
	    		    		 
	    		    		  if(m.getKey().equals(words[i])) {
	    		    			  int index = 0;
	    		    			  do {
	    		    				 
	    		    			    add = add +"("+(m.getKey()+" "+m.getValue().get(index))+")";
	    		    			    index++;
	    		    			  }while(m.getValue().size() > index );
	    		    			    inr++;
	    		    			    break;
	    		    			   
	    		    		  }
	    		    		   
	    		    	 }
	    		     }
	    		     listSet.add("("+word[0]+add+")");
	      		}
	    	
	      }
	      
	    if(!(listSet.size() == 0)) {  
	      Collections.reverse(listSet);
	      System.out.print("\nParse Tree:\t");
	      String s = "";
	      for(String li : listSet) {
	    	 s += li +"";
	      }
	      
	      	System.out.println("("+startSymbol+s+")");
	    }
	    else {
	    	System.out.println("\nParse Tree : String doesn't match, no Parse Tree");
	    }
	      
	      
	      int i =0;
	      int j =0;
	      
	      
	      for (int l = 1; l <= length; ++l)
	      {
	    	 String spaces = String.format("%"+l*2+"s", "");
	    	 System.out.println();
	    	 System.out.print(spaces);
	    	 while(i<length && j<length){
	    		 if(table[i][j].isEmpty()){
	    			 System.out.print("[-] ");
	    		 }else{
	    			 System.out.print(table[i][j]+" ");
	    		 }
	    		
	    		 i++;
	    		 j++;
	    	 }
	    	 i=0;
	    	 j= i+l;
	    	 //System.out.println();
	      }
	      
	      
	      if (table[0][length - 1].contains(startVariable.trim())) // started from 0
	         return true;
	      return false;
	   }

	  
	  public static void main(String ar[]) {
		  
		  String file = "/home/anurag/eclipse-workspace/CYKAlgorithmImplementation/grammar_1.txt";
		  
		  // Sentences from grammar one  
		  String sentence1 = "Mary saw Bob";
		  String sentence2 = "the dog saw the man in the park";
		  String sentence3 = "​the dog saw man in park";
		  
		  // Sentences from grammar two
		  String sentence4 = "Papa ate the caviar";
		  String sentence5 = "Papa ate the caviar with a spoon";
		  String sentence6 = "Papa ate the banana";
		  
		  //Sentence for the sample file attached with the mail
		  String sentence7 = "I shot an elephant in my pajamas";
		  
		  CYKImplementation cykImplement = CYKImplementation.getInstance();
		  cykImplement.processGrammarFile(file);
		  cykImplement.convertCfgtoCnf();
		  if (cykImplement.processString(sentence1.split(" ")))
		         System.out.println("\n"+"true : String matches");
		      else
		         System.out.println("\n"+"false : String doesn't match");
		   }
	}

